"""
Discord Alot Bot
https://hyperboleandahalf.blogspot.com/2010/04/alot-is-better-than-you-at-everything.html
"""

import json
import logging
import random
import discord
from discord.ext import commands
from utilities.config import *

# https://discord.com/api/oauth2/authorize?client_id=751832544722419825&permissions=67648&scope=bot

permission_bits = 67648
logging.basicConfig(level=logging.DEBUG)


async def get_prefix(bot: commands.Bot, message: discord.Message):
    """
    Determine if a guild has configured their own prefixes, otherwise return our defaults.
    """
    guild = message.guild  # type: discord.Guild
    extras = prefix_config.get(guild.id, default_prefixes)
    prefixes = commands.when_mentioned_or(*extras)(bot, message)
    logging.debug(f'Returning prefixes {prefixes} for guild {guild.name} ({guild.id})')
    return prefixes


async def update_prefix(guild_id, prefixes: list):
    prefix_config[guild_id] = prefixes


bot = commands.Bot(command_prefix=get_prefix)


@bot.event
async def on_ready():
    """ Notify when the bot is connected to Discord and ready """
    msg = f'Startup complete as bot {bot.user.name} ({bot.user.id})'
    print(msg)
    print('--------')
    # owner = await grabbot.fetch_user(grabbot.owner_id)
    # await owner.send(msg)


@bot.command()
async def ping(ctx):
    """ Simple ping command to see if the bot is responding """
    await ctx.send(f'Pong ({round(bot.latency * 1000)}ms)')


@bot.command()
@commands.guild_only()
@commands.has_guild_permissions(manage_guild=True)
async def prefix(ctx: commands.Context, message: discord.Message):
    """
    Allow changing the configured prefix.
    TODO: check permissions
    """
    allowed_prefixes = [
        '.', ',',
        '!', '?', '!?', '?!', '!!', '??',
        '$', '€', '£', '¥',
        '#', '%', '^', '&', '*',
        '(', ')', '_', '-', '+', '=',
    ]
    prefixes = ' '.split(message.content)
    for p in prefixes:
        if p not in allowed_prefixes:
            await ctx.send(f'Prefix {p} not in list of allowed prefixes')
            await ctx.send(f'Allowed prefixes: {", ".join(allowed_prefixes)}')
            return
    print(message)
    print(prefixes)
    await update_prefix(ctx.guild.id, prefixes)
    await ctx.send(f'Configured {prefixes} as the new prefix(es)')


@prefix.error
async def prefix_handler(ctx: commands.Context, error):
    current_prefixes = await get_prefix(bot, ctx.message)
    cps = ', '.join(current_prefixes[1:])
    await ctx.send(f'Current prefixes: {cps}')


@bot.command()
async def reload(ctx: commands.Context, cog=None):
    if not cog:
        await ctx.send(f'Loaded extensions: {bot.extensions}')

    if cog not in bot.extensions:
        await ctx.send(f'Could not find cog {cog} in loaded extensions')
        await ctx.send(f'Loaded extensions: {bot.extensions}')
        return
    bot.reload_extension(cog)
    await ctx.send(f'Reloaded extension {cog}')


@bot.command()
async def alotinvite(ctx: commands.Context):
    permissions = discord.Permissions(permission_bits)
    invite_url = discord.utils.oauth_url(client_id=get_config_key('client_id'), permissions=permissions)
    embed = discord.Embed(title='Invite link')
    embed.url = invite_url
    embed.description = 'Invite me to your server! Click the link above.'
    colours = random.sample(range(0, 255), 3)
    embed.colour = discord.Colour.from_rgb(*colours)
    await ctx.send(embed=embed)
    await ctx.message.delete(delay=4)


@bot.command()
async def shutdown(ctx: commands.Context):
    if await ctx.bot.is_owner(ctx.author):
        await ctx.send('Shutting down.')
        await bot.close()


if __name__ == '__main__':
    config = load_config()
    extensions = [
        'lot.grab',
    ]
    for ext in extensions:
        bot.load_extension(ext)

    bot.run(config['token'])
