"""
Discord.py Cog less-cluttered link embeds.

When someone posts a link with embeds enabled, a lot of space is frequently taken up by the "context": an image,
summary or description, and maybe more.
This Cog aims to reduce that cluttering, and requires you to disable embeds for members, but keep it enabled for this
bot.

It retrieves the page of a link that gets posted, extracts the page title,
TODO and optionally more
and posts it instead of a full-blown embed.
"""

import aiohttp
import asyncio
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import discord
from discord.ext import commands

context_channels = [
    751836187366260807,  # #grabbot in TestDiscordBDOHC
]


class LinkContext(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.Cog.listener
    async def on_message(self, message: discord.Message):
        channel = message.channel # type: discord.TextChannel
        if channel.id not in context_channels:
            return

        # Check if message starts with something that resembles a web link
        if not message.content.startswith('http'):
            return

        try:
            parsed_url = urlparse(message.content)
        except ValueError as ve:
            await channel.send(f'An error occurred: {ve}')
            return

        async with aiohttp.ClientSession() as session:
            async with session.get(parsed_url.geturl()) as resp:
                if resp.status == 200:
                    pass
                    # page_title =
                    # channel.send()
