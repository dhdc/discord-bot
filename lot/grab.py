"""
Discord.py Cog for grabbing and saving what someone said last.

Commands:
    - !grab <author> OR <message_id>
        Saves the last message from <author>, or the message contents in <message_id> as a quote
    - !quote <author>
        Displays the last saved message from <author>
    - !qrandom <author>
        Displays a random quote from <author>
    - !qlist <author>
        Lists all quotes from <author>
    - !qhelp
        Lists all commands

!grab funtionality
1)  command gets executed
2)  user provided verified
3)  last message from user gets found (with some limitations)
4)  create Quote object instance
5)  check if message ID is already saved in database
6)  save Quote object instance in database

Database design:
- quotes table
    - id primary key
    - message_id (int) (primary key?)
    - datetime of time that the quote got created
    - message (string) that got quoted
    - author (id or name? Both?) of the quote
    - creator (id or name?) of the quote
"""

import json
import logging
import random
import discord
from collections import namedtuple
from typing import Union
from pprint import pprint
from datetime import datetime
from discord.ext import commands

history_limit = 100


class Graba(commands.Cog):
    success_messages = [
        'Zoinks!',
        #'Saved _for all of **eternity**_'
    ]

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    class Quote:
        """
        Keep minimum amount of information
        ID's for integers, strings for names, no objects.
        We can deserialise later if so desired.
        """
        def __init__(self, message_id, timestamp, message, author, creator):
            self.message_id = message_id
            self.datetime = timestamp
            self.message = message
            self.author = author
            self.creator = creator

    @staticmethod
    async def save_quote(quote):
        logging.debug(f'Saving quote')
        pprint(quote, indent=2)

        with open('quotes.json', 'r+') as f:
            try:
                data = json.load(f)
                # logging.debug(json.dumps(quote, default=lambda quote: quote.__dict__))
                data.update(quote)
                f.seek(0)
                json.dump(data, f)
            except json.JSONDecodeError as jde:
                logging.error(jde.msg)
                return False

        return True

    @commands.command()
    async def grab(self, ctx: commands.Context, param: Union[discord.Member, discord.Message]):
        creator = ctx.author
        author = discord.Member
        message = discord.Message

        # TODO separate into function
        if isinstance(param, discord.Member):
            """
            Parameter passed was a user ID or @Mention. Proceed to find last message from user.
            Find the last message sent by author in the current channel.
            """
            author = param
            logging.debug(f'Found request to grab user {author.name} (id {author.id}) by {creator.name} (id {creator.id})')

            async def last_message_from_user(ctx, user):
                channel = ctx.channel  # type: discord.TextChannel
                async for msg in channel.history(limit=history_limit):
                    # History is sorted from newest to oldest by default.
                    if msg.author == author:
                        logging.debug(f'Found author message - author {msg.author}, message {msg.content}')
                        return msg

            message = await last_message_from_user(ctx, author)
            if not message:
                await ctx.send(f'{author.mention}: Could not find message by {author.display_name} '
                               f'in the last {history_limit} messages!')

        if isinstance(param, discord.Message):
            """
            Parameter passed was a message ID.
            Actually more efficient because we don't have to go through the channel history.
            """
            # message = await ctx.fetch_message(param.id)
            message = param
            author = message.author
            await ctx.send(f'Found request to grab message by ID {message.id} with content {message.content}')

        if creator == author:
            await ctx.send(f'{creator.mention} Cannot quote a message from yourself, dummy')
            # return # TODO: remove

        # quote = Quote(message.id, message.created_at.isoformat(), message.content, message.author.name, creator.name)
        quote = {
            "message_id": message.id,
            "timestamp": message.created_at.isoformat(),
            "message": message.content,
            "author": message.author.name,
            "creator": creator.name,
        }
        if await self.save_quote(quote):
            await ctx.send(f'{ctx.author.mention}: {random.choice(self.success_messages)}')
        else:
            await ctx.send(f'Something went wrong, please inform <@{ctx.bot.owner_id}>')

    @grab.error
    async def grab_handler(self, ctx, error):
        await ctx.send(f'{ctx.author.mention}: {error}')


def setup(bot):
    bot.add_cog(Graba(bot))
