import logging


def flatten():
    return lambda l: [item for sublist in l for item in sublist]


async def list_joiner(iterable, separator=', '):
    if len(iterable) > 1:
        return separator.join(iterable[:-1]) + iterable[-1]
    return iterable[0]
