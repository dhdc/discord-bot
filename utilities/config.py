import json
import discord

default_prefixes = ['!', '?']

prefix_config = {
    # Server id - [prefixes]
    328549432826265600: ['!', '^'],
}

otp_req_channel = discord.TextChannel
otp_mod_channel = discord.TextChannel


def load_config(config_file='config.json'):
    with open(config_file, 'r') as f:
        return json.load(f)


def get_config_key(key):
    return load_config()[key]


class ConfigAlot:
    def __init__(self):
        pass


class ServerConfig:
    guild: discord.Guild
    otp_req_channel: discord.TextChannel
    otp_mod_channel: discord.TextChannel

    def __init__(self):
        pass
